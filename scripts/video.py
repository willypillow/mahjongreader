import cv2 as cv
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='Code for Finding contours in your image tutorial.')
parser.add_argument('--input', help='Path to input image.', default='HappyFish.jpg')
args = parser.parse_args()
cap = cv.VideoCapture(args.input)
if cap is None:
    print('Could not open or find the image:', args.input)
    exit(0)

# thres = [75, 120, 40, 256, 55, 256]
thres = [23, 45, 30, 256, 70, 256]
maxWidth, maxHeight = 900, 400
def setThres(idx, val):
    global thres
    thres[idx] = val

# cv.namedWindow('Mask')
# cv.namedWindow('Bars')
# cv.createTrackbar('minH', 'Bars', thres[0], 256, lambda x: setThres(0, x))
# cv.createTrackbar('maxH', 'Bars', thres[1], 256, lambda x: setThres(1, x))
# cv.createTrackbar('minS', 'Bars', thres[2], 256, lambda x: setThres(2, x))
# cv.createTrackbar('maxS', 'Bars', thres[3], 256, lambda x: setThres(3, x))
# cv.createTrackbar('minV', 'Bars', thres[4], 256, lambda x: setThres(4, x))
# cv.createTrackbar('maxV', 'Bars', thres[5], 256, lambda x: setThres(5, x))

erode = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))
dilate = cv.getStructuringElement(cv.MORPH_RECT, (16, 16))
cnt = 0

while True:
    ret, src = cap.read()
    cnt += 1
    srcHsv = cv.cvtColor(src, cv.COLOR_BGR2HSV)
    srcRange = cv.inRange(srcHsv, np.array([thres[0], thres[2], thres[4]]), np.array([thres[1], thres[3], thres[5]]))
    srcRange = cv.erode(srcRange, erode, iterations = 2)
    srcRange = cv.dilate(srcRange, dilate, iterations = 2)
    srcRange = cv.erode(srcRange, dilate, iterations = 2)
    # srcRes = cv.bitwise_and(src, src, mask = srcRange)
    contours, hierarchy = cv.findContours(srcRange, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    if len(contours) != 4: continue
    tls, trs, bls, brs = [], [], [], []
    for i in range(len(contours)):
        tls.append(max(contours[i], key=lambda x: x[0][0] - x[0][1]))
        trs.append(max(contours[i], key=lambda x: x[0][0] + x[0][1]))
        bls.append(min(contours[i], key=lambda x: x[0][0] + x[0][1]))
        brs.append(min(contours[i], key=lambda x: x[0][0] - x[0][1]))
    tl = max(brs, key=lambda x: x[0][0] - x[0][1])
    tr = max(bls, key=lambda x: x[0][0] + x[0][1])
    bl = min(trs, key=lambda x: x[0][0] + x[0][1])
    br = min(tls, key=lambda x: x[0][0] - x[0][1])
    cornerPts = np.array([bl, tl, tr, br], np.float32)
    dstPts = np.array([[0,0], [maxWidth - 1, 0], [maxWidth - 1, maxHeight - 1], [0, maxHeight - 1]], np.float32)
    warpMat = cv.getPerspectiveTransform(cornerPts, dstPts)
    warp = cv.warpPerspective(src, warpMat, (maxWidth, maxHeight))
    w, h = maxWidth // 9, maxHeight // 4
    # for i in range(9):
    #     warp = cv.line(warp, (w * i, 0), (w * i, maxHeight - 1), (0, 255, 0), 2)
    # for i in range(4):
    #     warp = cv.line(warp, (0, h * i), (maxWidth - 1, h * i), (0, 255, 0), 2)
    for j in range(4):
        for i in range(9):
            cv.imwrite("{0}x{1}x{2}.tiff".format(i, j, cnt), warp[h * j : h * (j + 1), w * i : w * (i + 1)])
    # cv.imshow('Mask', warp)
    # cv.imshow('Bars', srcRange)
    # cv.waitKey(1)
