import itertools
import glob
import random
import imgaug as ia
from imgaug import augmenters as iaa
import numpy as np
import cv2 as cv
random.seed(120000709)

IMGH, IMGW, CARDH, CARDW = 608, 608, 100, 75
MIDX = (IMGW - CARDW) // 2
MIDY = (IMGH - CARDH) // 2
MARGIN = 15
shuuhai = list(itertools.product(range(0, 10), ['m', 'p', 's']))
jihai = list(itertools.product(range(1, 8), ['z']))
tiles = list(map(lambda s: str(s[0]) + s[1], shuuhai + jihai)) + ['bb']

imgs = {}
idx = {}
bgs = []
for i in range(len(tiles)):
    imgs[tiles[i]] = glob.glob(tiles[i] + "/*")
    idx[tiles[i]] = i
bgs = glob.glob("bgs/*")

ratio = iaa.Resize({ "height": CARDH, "width": CARDW })
transTile = iaa.Sequential([
    iaa.Crop(percent=(0, 0.1)),
    iaa.SomeOf((0, 5), [
        iaa.Sometimes(0.3,
            iaa.Superpixels(
                p_replace=(0, 1.0),
                n_segments=(20, 200)
            )
        ),
        iaa.OneOf([
            iaa.GaussianBlur((0, 3.0)),
            iaa.AverageBlur(k=(2, 7)),
            iaa.MedianBlur(k=(3, 11)),
        ]),
        iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5)),
        iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0)),
        iaa.Sometimes(0.3,
            iaa.OneOf([
                iaa.EdgeDetect(alpha=(0, 0.7)),
                iaa.DirectedEdgeDetect(
                    alpha=(0, 0.7), direction=(0.0, 1.0)
                )
            ])
        ),
        iaa.OneOf([
            iaa.Dropout((0.01, 0.1), per_channel=0.5),
            iaa.CoarseDropout((0.03, 0.15),
                size_percent=(0.02, 0.05),
                per_channel=0.2
            ),
        ]),
        iaa.Invert(0.05, per_channel=True),
        iaa.Multiply((0.5, 1.5), per_channel=0.2),
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
        iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5),
        iaa.Grayscale(alpha=(0.0, 0.7)),
        iaa.Sometimes(0.3,
            iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)
        ),
        iaa.Sometimes(0.3, iaa.PiecewiseAffine(scale=(0.01, 0.05)))
    ], random_order=True)
], random_order=True)
transBg = iaa.Sequential([
    iaa.Scale({ "height": IMGH, "width": IMGW }),
    iaa.WithColorspace(
        to_colorspace="HSV",
        from_colorspace="RGB",
        children=iaa.Sequential([
            iaa.WithChannels(0, iaa.Add((0, 255))),
            iaa.WithChannels(1, iaa.Add((0, 255)))
        ])
    ),
    iaa.Rot90((0, 3))
])
rotateTile = iaa.Affine(rotate=(-5, 5))
transform = iaa.Sequential([
    iaa.Pad(px=100, keep_size=False),
    iaa.Affine(scale={ "x": (0.3, 1.5), "y": (0.3, 1.5) }, rotate=(-180, 180), shear=(-16, 16), order=[0, 1]),
    iaa.PerspectiveTransform(scale=(0, 0.15)),
    iaa.Crop(px=100, keep_size=False),
    iaa.Rot90((0, 3))
])

def randTile(tile = ''):
    if tile == '':
        tile = random.choice(tiles)
    return tile, random.choice(imgs[tile])

def randBg():
    return random.choice(bgs)

def keypoint(x, y):
    return ia.KeypointsOnImage([
        ia.Keypoint(x=x, y=y),
        ia.Keypoint(x=x, y=y+CARDH),
        ia.Keypoint(x=x+CARDW, y=y+CARDH),
        ia.Keypoint(x=x+CARDW, y=y)
    ], shape=(IMGH, IMGW, 4))

def toAlpha(img):
    b, g, r = cv.split(img)
    a = np.ones(b.shape, dtype=b.dtype)
    return cv.merge((b, g, r, a))

def augment(img, kps, seq):
    myImg = seq.augment_image(img)
    myKps = seq.augment_keypoints([kps])[0]
    fail = False
    for kp in myKps.keypoints:
        if not (0 <= kp.x < IMGW and 0 <= kp.y < IMGH):
            fail = True
            break
    if not fail:
        return myImg, myKps
    raise ValueError(seq)

def attempt(tile1Bg, tile2Bg, kps1, kps2):
    for i in range(10):
        try:
            myTile2Bg, myKps2 = augment(tile2Bg, kps2, rotateTile.to_deterministic())
            tf = transform.to_deterministic()
            myTile1Bg, myKps1 = augment(tile1Bg, kps1, tf)
            myTile2Bg, myKps2 = augment(myTile2Bg, myKps2, tf)
            return myTile1Bg, myTile2Bg, myKps1, myKps2
        except ValueError as e:
            # print(e)
            pass
    raise ValueError

def genBB(kps):
    xs, ys = [kp.x for kp in kps.keypoints], [kp.y for kp in kps.keypoints]
    minX, maxX, minY, maxY = max(0, min(xs) - 3), min(IMGW - 1, max(xs) + 3), max(0, min(ys) + 3), min(IMGH - 1, max(ys) + 3)
    cenX, cenY = (minX + maxX) / 2, (minY + maxY) / 2
    w, h = maxX - minX, maxY - minY
    return cenX, cenY, w, h

def createScene1(name):
    while True:
        try:
            x1, y1 = random.randint(0, IMGW - 2 * CARDW), random.randint(0, IMGH - 2 * CARDH)
            x2, y2 = x1 + CARDW + random.randint(0, MARGIN), y1 + random.randint(-MARGIN, MARGIN)
            kps1, kps2 = keypoint(x1, y1), keypoint(x2, y2)
            class1, tile1Path = randTile()
            class2, tile2Path = randTile()
            tile1, tile2 = cv.imread(tile1Path), cv.imread(tile2Path)
            tile1Bg = np.zeros((IMGH, IMGW, 4), dtype=np.uint8)
            tile1 = ratio.augment_image(tile1)
            tile1 = transTile.augment_image(tile1)
            tile1 = toAlpha(tile1)
            tile1Bg[y1 : y1 + CARDH, x1 : x1 + CARDW, :] = tile1
            tile2Bg = np.zeros((IMGH, IMGW, 4), dtype=np.uint8)
            tile2 = ratio.augment_image(tile2)
            tile2 = transTile.augment_image(tile2)
            tile2 = toAlpha(tile2)
            tile2Bg[y2 : y2 + CARDH, x2 : x2 + CARDW, :] = tile2
            tile1Bg, tile2Bg, kps1, kps2 = attempt(tile1Bg, tile2Bg, kps1, kps2)
            alpha = 1.0
            if random.random() < 0.3: alpha = random.uniform(0.75, 1.0)
            bg = cv.imread(randBg())
            res = transBg.augment_image(bg)
            resAlpha = toAlpha(res)
            tile1Bg = cv.addWeighted(tile1Bg, alpha, resAlpha, 1.0 - alpha, 0)
            tile2Bg = cv.addWeighted(tile2Bg, alpha, resAlpha, 1.0 - alpha, 0)
            res = np.where(np.stack([tile1Bg[:,:,3]] * 3, -1), tile1Bg[:,:,0:3], res)
            res = np.where(np.stack([tile2Bg[:,:,3]] * 3, -1), tile2Bg[:,:,0:3], res)
            cx1, cy1, w1, h1 = genBB(kps1)
            cx2, cy2, w2, h2 = genBB(kps2)
            with open("{0}.txt".format(name), 'w') as f:
                f.write("{0} {1} {2} {3} {4}\n".format(idx[class1], cx1 / IMGW, cy1 / IMGH, w1 / IMGW, h1 / IMGH))
                f.write("{0} {1} {2} {3} {4}\n".format(idx[class2], cx2 / IMGW, cy2 / IMGH, w2 / IMGW, h2 / IMGH))
            # rect1 = (int(round(cx1 - w1 / 2)), int(round(cy1 - h1 / 2)), int(round(w1)), int(round(h1)))
            # rect2 = (int(round(cx2 - w2 / 2)), int(round(cy2 - h2 / 2)), int(round(w2)), int(round(h2)))
            # res = cv.rectangle(res, rect1, (0, 255, 0))
            # res = cv.rectangle(res, rect2, (0, 255, 0))
            cv.imwrite("{0}.jpg".format(name), res)
            return
        except ValueError as e:
            # print(e)
            pass

for i in range(200000):
    if i % 100 == 0: print(i)
    createScene1("{0}".format(i))

