import cv2 as cv
import numpy as np
import argparse
import random as rng
rng.seed(12345)

parser = argparse.ArgumentParser(description='Code for Finding contours in your image tutorial.')
parser.add_argument('--input', help='Path to input image.', default='HappyFish.jpg')
args = parser.parse_args()
src = cv.imread(cv.samples.findFile(args.input))
if src is None:
    print('Could not open or find the image:', args.input)
    exit(0)

thres = [23, 45, 31, 256, 75, 256]
def setThres(idx, val):
    global thres
    thres[idx] = val

srcHsv = cv.cvtColor(src, cv.COLOR_BGR2HSV)

cv.namedWindow('InRange')
cv.namedWindow('Bars')
cv.createTrackbar('minH', 'Bars', thres[0], 256, lambda x: setThres(0, x))
cv.createTrackbar('maxH', 'Bars', thres[1], 256, lambda x: setThres(1, x))
cv.createTrackbar('minS', 'Bars', thres[2], 256, lambda x: setThres(2, x))
cv.createTrackbar('maxS', 'Bars', thres[3], 256, lambda x: setThres(3, x))
cv.createTrackbar('minV', 'Bars', thres[4], 256, lambda x: setThres(4, x))
cv.createTrackbar('maxV', 'Bars', thres[5], 256, lambda x: setThres(5, x))

while True:
    srcRange = cv.inRange(srcHsv, np.array([thres[0], thres[2], thres[4]]), np.array([thres[1], thres[3], thres[5]]))
    erode = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))
    dilate = cv.getStructuringElement(cv.MORPH_RECT, (16, 16))
    srcRange = cv.erode(srcRange, erode, iterations = 2)
    srcRange = cv.dilate(srcRange, dilate, iterations = 2)
    srcRange = cv.erode(srcRange, dilate, iterations = 2)
    srcRes = cv.bitwise_and(src, src, mask = srcRange)
    contours, hierarchy = cv.findContours(srcRange, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    print(len(contours))
    cv.imshow('InRange', srcRes)
    cv.waitKey(1)
