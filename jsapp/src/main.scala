import com.thoughtworks.binding.Binding.{ Constants, Var, Vars }
import com.thoughtworks.binding.{ Binding, dom }
import org.scalajs.{ dom => domjs }, domjs._
import org.scalajs.dom.ext._
import scala.scalajs.js
import scala.concurrent.{ Future, Promise }
import scala.util.{ Success, Failure }
import scala.scalajs.js.annotation.JSExportTopLevel
import scala.concurrent.ExecutionContext
import pw.nerde.riichala._

object Adapter {
  val Kaze: Map[String, Kazehai] =
    Map("east" -> Ton, "south" -> Nan, "west" -> Sha, "north" -> Pei)

  def mkTile(tileType: Char, i: Int): Tile = tileType match {
    case c if c == 'm' && 1 <= i && i <= 9 => Manzu(i)
    case c if c == 'p' && 1 <= i && i <= 9 => Pinzu(i)
    case c if c == 's' && 1 <= i && i <= 9 => Souzu(i)
    case c if c == 'z' && i == 1           => Ton
    case c if c == 'z' && i == 2           => Nan
    case c if c == 'z' && i == 3           => Sha
    case c if c == 'z' && i == 4           => Pei
    case c if c == 'z' && i == 5           => Haku
    case c if c == 'z' && i == 6           => Hatsu
    case c if c == 'z' && i == 7           => Chun
  }

  def mkHand(str: String): (Seq[Tile], Seq[Mentsu]) = {
    val (emTile, emMend) = (List.empty[Tile], List.empty[Mentsu])
    val res = str.reverse.foldLeft(('z', emTile, emTile, emMend)) {
      case ((tileType, mentsuAcc, handAcc, fuuroAcc), cur) =>
        cur match {
          case c if Seq('m', 'p', 's', 'z') contains c =>
            (c, mentsuAcc, handAcc, fuuroAcc)
          case i if '1' to '9' contains i =>
            val tile = mkTile(tileType, i - '0')
            (tileType, tile :: mentsuAcc, handAcc, fuuroAcc)
          case ')' | ']' =>
            (tileType, List.empty, mentsuAcc ++ handAcc, fuuroAcc)
          case '(' =>
            val newMentsu = HandElement.from(mentsuAcc.sorted).asInstanceOf[Mentsu]
            (tileType, List.empty, handAcc, newMentsu +: fuuroAcc)
          case '[' =>
            val newMentsu = Kantsu(mentsuAcc.sorted, true)
            (tileType, List.empty, handAcc, newMentsu +: fuuroAcc)
          case _ =>
            (tileType, mentsuAcc, handAcc, fuuroAcc)
        }
    }
    (res._2 ++ res._3, res._4)
  }

  def mkSeq(str: String): Seq[Seq[String]] = {
    val (emTile, emMend) = (List.empty[String], List.empty[List[String]])
    val res = str.reverse.foldLeft(('z', emTile, emTile, emMend)) {
      case ((tileType, mentsuAcc, handAcc, fuuroAcc), cur) =>
        cur match {
          case c if Seq('m', 'p', 's', 'z') contains c =>
            (c, mentsuAcc, handAcc, fuuroAcc)
          case i if '1' to '9' contains i =>
            (tileType, s"${i}${tileType}" :: mentsuAcc, handAcc, fuuroAcc)
          case ')' | ']' =>
            (tileType, List.empty, mentsuAcc ++ handAcc, fuuroAcc)
          case '(' | '[' =>
            (tileType, List.empty, handAcc, mentsuAcc +: fuuroAcc)
          case _ =>
            (tileType, mentsuAcc, handAcc, fuuroAcc)
        }
    }
    (res._2 ++ res._3) :: res._4
  }

  def apply(
      handStr: String,
      agariStr: String,
      akadora: String,
      doraStr: String,
      tsumo: Boolean,
      seatStr: String,
      roundStr: String,
      special: Seq[Yaku.SpecialYaku]): ScoreCalculator.Result = {
    print("abc")
    print(handStr)
    print(agariStr)
    print(akadora)
    try {
      val (handTuple, doras) = (mkHand(handStr), mkHand(doraStr)._1)
      val kaze = Seq(Kaze(seatStr), Kaze(roundStr))
      val hand = Hand(handTuple._1, handTuple._2)
      val oya = seatStr == "east"
      val agaripai = mkTile(agariStr(1), agariStr(0) - '0')
      val winningHand =
        WinningHand(hand, tsumo, oya, agaripai, special, akadora.toInt)
      ScoreCalculator(winningHand, kaze, doras)
    } catch {
      case e: Exception =>
        ScoreCalculator.Result(
          0,
          NoLimit,
          ScoreCalculator.NoAgari,
          Seq.empty,
          Seq.empty)
    }
  }

  def resToStr(result: ScoreCalculator.Result): (String, String, String, String) = {
    val yakuCnt = result.yakus.map(_._2).sum
    val fuCnt = result.fus.map(_._2).sum
    val cost = result.cost match {
      case ScoreCalculator.OyaTsumo(a) => f"$a"
      case ScoreCalculator.Tsumo(a, b) => f"$a-$b"
      case ScoreCalculator.Ron(a)      => f"$a"
      case _                           => "0"
    }
    (yakuCnt.toString, fuCnt.toString, result.limit.toString, cost)
  }

  def updateScore(
      result: Vars[ScoreCalculator.Result],
      handStr: String,
      agariStr: String,
      akadora: String,
      doraStr: String,
      tsumo: html.Input,
      riichi: html.Input,
      doubleriichi: html.Input,
      ippatsu: html.Input,
      haitei: html.Input,
      rinshan: html.Input,
      chankan: html.Input,
      tenhou: html.Input) {
    val seat = document.getElementsByName("seat").collect {
      case i: html.Input if i.checked => i.value
    }.head
    val round = document.getElementsByName("round").collect {
      case i: html.Input if i.checked => i.value
    }.head
    val special =
      (if (riichi.checked) Seq(Yaku.Riichi) else Seq()) ++
      (if (doubleriichi.checked) Seq(Yaku.DoubleRiichi) else Seq()) ++
      (if (ippatsu.checked) Seq(Yaku.Ippatsu) else Seq()) ++
      (if (haitei.checked) Seq(Yaku.Haitei) else Seq()) ++
      (if (rinshan.checked) Seq(Yaku.Rinshan) else Seq()) ++
      (if (chankan.checked) Seq(Yaku.Chankan) else Seq()) ++
      (if (tenhou.checked) Seq(Yaku.Tenhou) else Seq())
    result.value.clear()
    result.value +=
      Adapter(
        handStr,
        agariStr,
        akadora,
        doraStr,
        tsumo.checked,
        seat,
        round,
        special)
  }

  def updateEff(result: Vars[(String, Int, Int)], handStr: String) {
    result.value.clear()
    handStr.split("\n")
      .map { line =>
        val arr = line.split(",")
        (arr(0), arr(1).toInt, arr(2).toInt)
      }
      .sortBy(x => (x._2, -x._3, x._1))
      .foreach(result.value += _)
  }

  def upload(): Future[XMLHttpRequest] = {
    implicit val ec = ExecutionContext.global
    val file = document.getElementById("file").asInstanceOf[html.Input].files(0)
    val canvas = document.createElement("canvas").asInstanceOf[html.Canvas]
    val ctx = canvas.getContext("2d").asInstanceOf[raw.CanvasRenderingContext2D]
    canvas.width = 864
    canvas.height = 864
    val promise = Promise[FormData]()
    val img = document.createElement("img").asInstanceOf[html.Image]
    img.src = raw.URL.createObjectURL(file)
    img.onload = { _: Event =>
      ctx.drawImage(img, 0, 0, 864, 864)
      val dataUrl = canvas.toDataURL("image/png")
      val binStr = window.atob(dataUrl.split(",")(1))
      val buf = new js.typedarray.Uint8Array(binStr.size)
      for (i <- 0 until binStr.size) buf(i) = binStr(i).toShort
      val blob = new raw.Blob(js.Array(buf))
      val formData = new FormData()
      formData.append("file", blob)
      promise.success(formData)
    }
    promise.future.flatMap { formData => 
      Ajax.post("recognition", Ajax.InputData.formdata2ajax(formData))
    }
  }

  def parseResp(str: String): (String, String, Int) = {
    val Array(handStr, agari) = str.split("\n")
    (handStr.replace('0', '5'), agari, handStr.count(_ == '0'))
  }
  
  def queryEff(hand: String): Future[XMLHttpRequest] = {
    Ajax.get(s"efficiency?hand=${hand}")
  }

  def updateHandVar(handVar: Vars[Vars[String]], handStr: String) {
    handVar.value.clear()
    mkSeq(handStr).map(s => Vars(s: _*)).foreach(handVar.value += _)
  }
}

object Ui {
  val translate: Map[String, String] = Map(
    "NoAgari" -> "未和",
    "NoLimit" -> "",
    "Mangan" -> "滿貫",
    "Haneman" -> "跳滿",
    "Baiman" -> "倍滿",
    "Sanbaiman" -> "三倍滿",
    "Yakuman" -> "役滿"
    )
}

class Ui {
  implicit val ec = ExecutionContext.global
  val handVar: Vars[Vars[String]] = Vars(Vars())
  val result: Vars[ScoreCalculator.Result] = Vars()
  val resultEff: Vars[(String, Int, Int)] = Vars()

  def setClassDisplay(name: String, value: String) {
      for (e <- document.getElementsByClassName(name)) {
        e.asInstanceOf[domjs.raw.HTMLElement].style.display = value
      }
  }

  @dom
  def form = {
    <div class="upload columns">
      <input id="file" class="column col-8 form-input" type="file" />
      <button class="btn col-2" onclick={ e: Event =>
            Adapter.upload().onComplete {
              case Success(resp) =>
                val (handRes, _, _) = Adapter.parseResp(resp.responseText)
                handEff.value = handRes
                Adapter.updateHandVar(handVar, handEff.value)
                setClassDisplay("efficiency-calc", "block")
                setClassDisplay("loading", "none")
              case _ => ;
            }
            setClassDisplay("hide-when-load", "none")
            setClassDisplay("loading", "block")
          }>
        牌效率
      </button>
      <button class="btn btn-primary col-2" onclick={ e: Event =>
            Adapter.upload().onComplete {
              case Success(resp) =>
                val (handRes, agariRes, akaRes) = Adapter.parseResp(resp.responseText)
                hand.value = handRes
                Adapter.updateHandVar(handVar, handEff.value)
                agaripai.value = agariRes
                aka.value = akaRes.toString
                setClassDisplay("loading", "none")
                setClassDisplay("score-calc", "block")
              case _ => ;
            }
            setClassDisplay("hide-when-load", "none")
            setClassDisplay("loading", "block")
          }>
        計分
      </button>
    </div>
    <div>
    {
      for (row <- handVar) yield {
        <div class="columns tile-div">
        {
          for (tile <- row) yield {
            <img class="tile" src={ s"https://up.nerde.pw/s/Regular/${tile}.svg" } />
          }
        }
        </div>
      }
    }
    </div>
    <div class="form-horizontal efficiency-calc hide-when-load">
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">手牌</label>
        </div>
        <div class="col-7 col-sm-12">
          <input class="form-input" id="handEff" type="text" />
        </div>
        <button class="col-2 col-sm-12 btn btn-primary" onclick={ _: Event =>
            Adapter.updateHandVar(handVar, handEff.value)
            setClassDisplay("loading", "block")
            Adapter.queryEff(handEff.value).onComplete {
              case Success(resp) =>
                setClassDisplay("loading", "none")
                Adapter.updateEff(resultEff, resp.responseText)
              case _ => ;
            }
            ;
          }>
          確認
        </button>
      </div>
    </div>
    <div class="efficiency-calc hide-when-load">
    {
      for (choice <- resultEff) yield {
        <div class="columns tile-div">
          <img class="tile" src={ s"https://up.nerde.pw/s/Regular/${choice._1}.svg" } />
          <span>{ choice._2 + " 向聽，" + choice._3 + " 進張" }</span>
        </div>
      }
    }
    </div>
    <div class="form-horizontal score-calc hide-when-load">
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">手牌</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="hand" type="text" />
        </div>
      </div>
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">和牌</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="agaripai" type="text" />
        </div>
      </div>
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">赤牌</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="aka" type="number" />
        </div>
      </div>
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">寶牌指示牌</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="dora" type="text" />
        </div>
      </div>
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">自風</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-radio form-inline">
            <input name="seat" value="east" type="radio" checked={true} />
            <i class="form-icon"></i>東
          </label>
          <label class="form-radio form-inline">
            <input name="seat" value="south" type="radio" />
            <i class="form-icon"></i>南
          </label>
          <label class="form-radio form-inline">
            <input name="seat" value="west" type="radio" />
            <i class="form-icon"></i>西
          </label>
          <label class="form-radio form-inline">
            <input name="seat" value="north" type="radio" />
            <i class="form-icon"></i>北
          </label>
        </div>
      </div>
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">場風</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-radio form-inline">
            <input name="round" value="east" type="radio" checked={true} />
            <i class="form-icon"></i>東
          </label>
          <label class="form-radio form-inline">
            <input name="round" value="south" type="radio" />
            <i class="form-icon"></i>南
          </label>
          <label class="form-radio form-inline">
            <input name="round" value="west" type="radio" />
            <i class="form-icon"></i>西
          </label>
          <label class="form-radio form-inline">
            <input name="round" value="north" type="radio" />
            <i class="form-icon"></i>北
          </label>
        </div>
      </div>
      <div class="columns form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">其它</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-switch form-inline">
            <input type="checkbox" id="tsumo" />
            <i class="form-icon"></i>自摸
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="riichi" />
            <i class="form-icon"></i>立直
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="doubleriichi" />
            <i class="form-icon"></i>雙立直
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="ippatsu" />
            <i class="form-icon"></i>一發
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="haitei" />
            <i class="form-icon"></i>海底
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="rinshan" />
            <i class="form-icon"></i>嶺上
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="chankan" />
            <i class="form-icon"></i>搶槓
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="tenhou" />
            <i class="form-icon"></i>天和
          </label>
        </div>
      </div>
      <button class="btn btn-primary" onclick={ _: Event =>
          Adapter.updateHandVar(handVar, hand.value)
          Adapter.updateScore(result, hand.value, agaripai.value, aka.value, dora.value, tsumo, riichi, doubleriichi, ippatsu, haitei, rinshan, chankan, tenhou)
        }>
        確認
      </button>
    </div>
    <div class="score-calc hide-when-load">
      <p>
      {
        for (r <- result) yield {
          <h2 class="text-center">
          {
            val (yakuCnt, fuCnt, limit, cost) = Adapter.resToStr(r) 
            f"${yakuCnt} 飜, ${fuCnt} 符, ${Ui.translate(limit)}, $cost"
          }
          </h2>
        }
      }
      </p>
      <div class="columns">
        <div class="col-6 col-sm-12">
          <table class="table">
            <thead>
              <tr>
                <th>役</th>
                <th>飜</th>
              </tr>
            </thead>
            <tbody>
            {
              Constants(result.bind.flatMap(_.yakus): _*).map { y =>
                <tr>
                  <td>{ y._1.toString }</td>
                  <td>{ y._2.toString }</td>
                </tr>
              }
            }
            </tbody>
          </table>
        </div>
        <div class="col-6 col-sm-12">
          <table class="table">
            <thead>
              <tr>
                <th>符</th>
                <th>數量</th>
              </tr>
            </thead>
            <tbody>
            {
              Constants(result.bind.flatMap(_.fus): _*).map { f =>
                <tr>
                  <td>{ f._1.toString }</td>
                  <td>{ f._2.toString }</td>
                </tr>
              }
            }
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="loading loading-lg"> <!-- --> </div>
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    val ui = new Ui
    dom.render(document.getElementById("form"), ui.form)
  }
}
