import sys

def getinput():
        # inputstring = list(input())
        inputstring = list(sys.argv[1])
        # print("input string is ", inputstring)
        inputstring.reverse()
        card = []
        cardtype = 'X'
        for i in inputstring:
                if i >= '0' and i <= '9':
                        if i == '0':
                                i = '5'
                        card.append(i + cardtype)
                else:
                        cardtype = i
        if len(card) != 14:
                # print("Input Size Error")
                exit(1)
        return card

def trs(s): # mpsz
        if s[1] == 'm': return (1 << ( 0 + int(s[0])))
        if s[1] == 'p': return (1 << (10 + int(s[0])))
        if s[1] == 's': return (1 << (20 + int(s[0])))
        if s[1] == 'z': return (1 << (30 + int(s[0])))

def retrs(s):
        if s < 10: return str(s -  0) + 'm'
        if s < 20: return str(s - 10) + 'p'
        if s < 30: return str(s - 20) + 's'
        if s < 40: return str(s - 30) + 'z'

def tin(a , b): # can be a set
        if a == b:
                return 1
        if a[1] == 'z' or b[1] == 'z' or a[1] != b[1]:
                return 0
        if abs(int(b[0]) - int(a[0])) <= 2:
                return 1
        return 0

def tin_cnt(a , b):   # howmany card c can let a, b, c be set
        # assert tin(a , b) == 1
        if a[0] > b[0]:
                a , b = b , a
        if int(b[0]) - int(a[0]) == 0:
                return trs(a)
        if int(b[0]) - int(a[0]) == 2:
                return (trs(a) << 1)

        val = 0
        if a[0] != '1':
                val |= (trs(a) >> 1)
        if b[0] != '9':
                val |= (trs(b) << 1)
        return val

def calcteam2(a , b): # can a, b be pair
        if a == b:
                return [0 , 0]
        if a != b:
                return [1 , trs(a) | trs(b)]

def calcteam3(a , b , c):
        if a[0] > b[0]:
                a , b = b , a
        if a[0] > c[0]:
                a , c = c , a
        if b[0] > c[0]:
                b , c = c , b

        aa = int(a[0])
        bb = int(b[0])
        cc = int(c[0])

        if a == b and b == c:
                return [0 , 0]
        if a[1] == b[1] and b[1] == c[1] and a[1] != 'z' and aa + 1 == bb and bb + 1 == cc:
                return [0 , 0]

        cnt = 0
        if tin(a , b): cnt |= tin_cnt(a , b)
        if tin(b , c): cnt |= tin_cnt(b , c)
        if tin(c , a): cnt |= tin_cnt(c , a)

        if cnt != 0:
                return [1 , cnt]
        else:
                pat = trs(a) | trs(b) | trs(c)
                for it in [a , b , c]:
                        if it[1] != 'z':
                                if int(it[0]) >= 3 : pat |= (trs(it) >> 2)
                                if int(it[0]) >= 2 : pat |= (trs(it) >> 1)
                                if int(it[0]) <= 8 : pat |= (trs(it) << 1)
                                if int(it[0]) <= 7 : pat |= (trs(it) << 2)
                return [2 , pat]

def update(a , b):
        if a[0] != b[0] and a[0] < b[0]:
                return a
        if a[0] != b[0] and a[0] > b[0]:
                return b
        return [a[0] , (a[1] | b[1])]

def calc_normal(card):

        n = len(card)
        # dp = {}
        # for i in range(1 << n):
        #       dp[i] = [99 , 0]
        # dp[0] = [0 , 0]

        dp = [[99, 0] for i in range(1 << n)]

        dp[0] = [0, 0]
        ans = [99, 0]
        for i in range(1 , 1 << n):
                v1 = 0
                bs = 0
                for s1 in range(n):
                        if (i & (1 << s1)) != 0:
                                bs += 1
                                v1 = s1

                if bs % 3 != 0: continue

                dp[i] = [99 , 0]
                for v2 in range(1 , v1):
                        if (i & (1 << v2)) == 0: continue

                        for v3 in range(0 , v2):
                                if (i & (1 << v3)) == 0: continue

                                pat = i ^ (1 << v1) ^ (1 << v2) ^ (1 << v3)
                                cost = calcteam3(card[v1] , card[v2] , card[v3]);
                                # first_Part = dp[i][0] + cost[0]
                                # secondPart = dp[i][1] | cost[1]

                                # if first_Part < dp[pat][0]:
                                #       dp[pat] = [first_Part , secondPart]
                                # if first_Part == dp[pat][0]:
                                #       dp[pat][1] |= secondPart
                                dp[i] = update(dp[i] , [dp[pat][0] + cost[0] , dp[pat][1] | cost[1]])

        for i in range(n):
                tmp = dp[((1 << n) - 1) ^ (1 << i)]
                ans = update(ans , [tmp[0] , tmp[1] | trs(card[i])])

        for i in range(1 << n):
                one = []
                zero = []
                for j in range(n):
                        if i & (1 << j) != 0:
                                one.append(j)
                        else:
                                zero.append(j)

                if len(one) == 9:
                        for a, b, c, d in [[0, 1, 2, 3], [0, 2, 1, 3], [0, 3, 1, 2]]:
                                v1 = card[zero[a]]
                                v2 = card[zero[b]]
                                v3 = card[zero[c]]
                                v4 = card[zero[d]]

                                if v1 == v2 and v3 == v4 and v1 != v3:
                                        tmp = trs(v1) | trs(v3)
                                        ans = update(ans , [dp[i][0] , dp[i][1] | tmp])

                                pair = 0


                                if v1 != v2 and v3 != v4:
                                        if tin(v1 , v2):
                                                pair |= trs(v3) | trs(v4)
                                        if tin(v3 , v4):
                                                pair |= trs(v1) | trs(v2)

                                if tin(v1 , v2):
                                        add = tin_cnt(v1 , v2)
                                        tmp = calcteam2(v3 , v4)
                                        ans = update(ans , [dp[i][0] + tmp[0] , dp[i][1] | tmp[1] | add |
pair])

                                if tin(v3 , v4):
                                        add = tin_cnt(v3 , v4)
                                        tmp = calcteam2(v1 , v2)
                                        ans = update(ans , [dp[i][0] + tmp[0] , dp[i][1] | tmp[1] | add |
pair])
        # print("normal return = " , ans)
        return ans

def calc_kokushi(card):
        kind = 0
        pair = 0
        ypair = 0
        npair = 0

        cc = {}
        for i in card:
                if trs(i) in cc :
                        cc[trs(i)] += 1
                else:
                        cc[trs(i)] = 1

        for i in ["1m", "1p", "1s", "9m", "9p", "9s"] + [str(c) + "z" for c in range(1 , 8)]:
                val = cc.get(trs(i) , 0)
                npair |= trs(i)
                if val == 0:
                        ypair |= trs(i)
                if val >= 1:
                        kind += 1
                if val >= 2:
                        pair = 1

        if pair == 0:
                # print("calc_kokushi return = " , [13 - kind - pair, npair])
                return [13 - kind - pair, npair]
        if pair == 1:
                # print("calc_kokushi return = " , [13 - kind - pair, ypair])
                return [13 - kind - pair, ypair]


def calc_7pair(card):
        cc = {}
        one = 0
        two = 0

        for i in card:
                if trs(i) in cc :
                        cc[trs(i)] += 1
                else:
                        cc[trs(i)] = 1

        for i in range(60):
                cnt = cc.get((1 << i) , 0)
                if cnt == 1:
                        one |= (1 << i)
                elif cnt >= 2:
                        two += 1
        # print("7pair return = " , [6 - two , one])
        return [6 - two , one]

card = getinput()
idx = 0
ans = [99 , 0]
bestin = []
tried = set()
for i in range(0, len(card)):
        if card[i] in tried:
            continue
        tried.add(card[i]) 

        tmpcard = card.copy()
        del tmpcard[i]

        # print("tmpcard = " , tmpcard)

        res = [99 , 0]

        res = update(res , calc_normal(tmpcard))
        res = update(res , calc_7pair(tmpcard))
        res = update(res , calc_kokushi(tmpcard))

        cc = {}
        can = 0
        for j in card:
                if trs(j) in cc :
                        cc[trs(j)] += 1
                else:
                        cc[trs(j)] = 1

        cantin = []
        # print("befor count = res" , res)
        for j in range(60):
                if res[1] & (1 << j):
                        # print("find j" , retrs(j))
                        can += 4 - cc.get((1 << j) , 0)

                        if 4 - cc.get((1 << j) , 0) > 0:
                                cantin += [retrs(j)]
        res[1] = can

        # print("remove " , card[i] , "res = " , res)
        # print("can in = " , cantin)
        print("{0},{1},{2}".format(card[i], res[0], res[1]))

        if res[0] < ans[0] or (res[0] == ans[0] and res[1] >= ans[1]):
                ans = res
                idx = i
                bestin = cantin

         # print("choose = " , card[i] , "Ans =" , res)

# print("=========================result=========================")
# print("Should throw = " , card[idx] , "Ans =" , ans)
# print("want = " , bestin)
