# Mahjong Reader #
A Riichi mahjong recognition system based on YOLOv3, with scoring and tile efficiency capabilities.

## Overview ##
- `server/`: Backend for the service. Being written in Scala, it utilizes Finch for HTTP handling, OpenCV for DNN inference, and invokes `efficiency.py` for tile efficiency calculation. For the ease of testing, it can also serve static files for the frontend.
- `jsapp/`: Frontend based on Scala.js and Spectre.css, utilizing [Riichala](https://gitlab.com/WillyPillow/riichala) for client-side score calculation.
- `scripts`: Various scripts used while training the model, see the Training section below.

## Building ##
1. Prerequsites:
	- [Mill](http://www.lihaoyi.com/mill/)
	- OpenCV
	- Python 3
2. Replace `https://up.nerde.pw/*` in `jsapp/` with the URLs of your own tile graphics. (The ones are my server are from <https://github.com/FluffyStuff/riichi-mahjong-tiles>.)
3. Place the OpenCV JAR in `server/lib/` 
4. `mill server.assembly`
5. The assembled JAR is at `out/server/assembly/dest/out.jar`. During deployment, you need to make sure that the OpenCV `.dll` / `.so` is on the same location as the build machine, and that `yolov3.label`, `yolov3.weights`, `yolov3.cfg`, and `efficiency.py` is placed in the working directory.

(`yolov3.weights` can be downloaded at <https://up.nerde.pw/s/yolov3.weights>.)

## Training ##
The training procedure is largely inspired by <https://github.com/EdjeElectronics/OpenCV-Playing-Card-Detector/> and <https://github.com/AlexeyAB/darknet#how-to-train-to-detect-your-custom-objects>.

For the data, we place the tiles in a grid with yellow stickers on four corners, and record videos at different angles. OpenCV is then used to capture the location of the four stickers, thereby locating each tile and extracting them to separate images. (`scripts/image.py` is for adjusting the color thresholds, and `scripts/video.py` is for the actual extraction.) Different tiles are then grouped into corresponding directories via a simple shell script.

As for augmentation, textures from <http://www.cb.uu.se/~gustaf/texture/> and <https://www.robots.ox.ac.uk/~vgg/data/dtd/> are used as backgrounds, and the images of the tiles (after individual augmentation such as cropping and noise addition) are pasted pairwise, with differing distances and relative angles, onto the background, with an alpha of 0.7 ~ 1 to acheive better resistence to light reflections. (See `gen.py` for details.) A total of 200000 images are generated, with 80% of them used for training and 20% used for validation.

YOLOv3-spp in [AlexeyAB's fork of Darknet](https://github.com/AlexeyAB/darknet) is used as the model for recognition. The training is based on a [pre-trained model](https://github.com/AlexeyAB/darknet#pre-trained-models), with `{width,height}=608`, `flip=0`, `classes=38`, `filters=129` ({0..9}{m,p,s} {1..7}z back-of-the-tile), and other parameters set to default.

The learning rate is manually lowered by a factor of 10 at iterations 13000 and 17000 respectively, due to the stagnating loss function and mAP on the validation set.

The final model uses the weights from iteration 24000 (early-stopping determined by mAP on validation set), and has a mAP of ~91% on the validation set. The procedure took about 2 days on a GTX1080Ti.

## Known Issues / Todo ##
- The code needs some documentation and cleaning up.
- Improve the build system so that the weights and incorporated in the assembled JAR.
- Move the tile efficiency module into its own microservice in Python instead of calling it via `sys.process`. Alternatively, rewrite it in Scala.
- I18n of frontend.
- Better error handling on the frontend instead of failing silently.
- Dockerfile for easier deployment.
- More details on the training section above.
