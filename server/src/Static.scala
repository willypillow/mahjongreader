package pw.nerde.mahjongreader

import com.twitter.finagle.http.Response
import com.twitter.io.{ Reader, Buf }
import io.finch._, io.finch.syntax._
import java.io.File

object Static {
  val html: Endpoint[Response] = get(/) {
    val htmlReader = Reader.fromFile(new File(getClass.getResource("/index.html").toURI))
    Reader.readAll(htmlReader).map(b => Ok(b).toResponse[Text.Html])
  }

  val js: Endpoint[Buf] = get("app.js") {
    val jsReader = Reader.fromFile(new File(getClass.getResource("/app.js").toURI))
    Reader.readAll(jsReader).map(b => Ok(b).withHeader("Content-Type" -> "application/javascript"))
  }
}
