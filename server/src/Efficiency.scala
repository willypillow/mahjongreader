package pw.nerde.mahjongreader

import io.finch._, io.finch.syntax._
import sys.process._

object Efficiency {
  val endpoint: Endpoint[String] =
    get("efficiency" :: param("hand")) { hand: String =>
      val proc = Process("python3", Seq("efficiency.py", hand))
      Ok(proc.!!)
    }
}
