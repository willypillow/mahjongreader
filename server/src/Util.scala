package pw.nerde.mahjongreader

object Util {
  class UnionFind(n: Int) {
    val parent = (0 until n).toArray
    
    def find(x: Int): Int = {
      if (parent(x) != x) parent(x) = find(parent(x))
      parent(x)
    }

    def join(x: Int, y: Int) {
      val (px, py) = (find(x), find(y))
      parent(px) = py
    }
  }
}
