package pw.nerde.mahjongreader

import com.twitter.conversions.StorageUnitOps._
import com.twitter.finagle.Http
import com.twitter.util.Await
import io.finch._, io.finch.syntax._
import org.opencv.core.Core

object Main extends App {
  System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  val s = Bootstrap
    .serve[Text.Plain](Recognition.endpoint)
    .serve[Text.Plain](Efficiency.endpoint)
    .serve[Text.Plain](Static.html :+: Static.js)
    .toService
  Await.ready(Http.server.withMaxRequestSize(10.megabytes).serve(":8082", s))
}
