package pw.nerde.mahjongreader

import com.twitter.finagle._
import com.twitter.io.Buf
import io.finch._, io.finch.syntax._
import org.opencv.core._
import org.opencv.dnn.Dnn
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import Util.UnionFind

object Recognition {
  val confThres = 0.25f
  val nmsThres = 0.4f
  val labelPath = "yolov3.label"
  val weightPath = "yolov3.weights"
  val configPath = "yolov3.cfg"
  val labels = {
    val labelSrc = scala.io.Source.fromFile(labelPath)
    try labelSrc.getLines.toArray finally labelSrc.close()
  }
  val net = Dnn.readNetFromDarknet(configPath, weightPath)
  val outputNames: List[String] = net.getUnconnectedOutLayersNames.asScala.toList

  def endpoint: Endpoint[String] =
    post("recognition" :: multipartFileUpload("file")) { file: http.exp.Multipart.FileUpload =>
      val image = file match {
        case d: http.exp.Multipart.OnDiskFileUpload =>
          Imgcodecs.imread(d.content.getCanonicalPath)
        case m: http.exp.Multipart.InMemoryFileUpload =>
          val bytes = Buf.ByteArray.Owned.extract(m.content)
          Imgcodecs.imdecode(new MatOfByte(bytes: _*), Imgcodecs.IMREAD_COLOR)
      }
      println(image.size())
      val resized = new Mat()
      Imgproc.resize(image, resized, new Size(864, 864), 0, 0, Imgproc.INTER_AREA)
      val blob = Dnn.blobFromImage(resized, 1.0 / 255, new Size(864, 864), new Scalar(0), true, false)
      val resultList = ListBuffer.empty[Mat].asJava
      net.setInput(blob)
      net.forward(resultList, outputNames.asJava)
      var (ids, confs, rects) = (List.empty[Int], List.empty[Float], List.empty[Rect])
      for (detect <- resultList.asScala) {
        for (i <- 0 until detect.rows) {
          val data = Array.fill(detect.cols * detect.channels)(0f)
          detect.get(i, 0, data)
          val max = data.drop(5).zipWithIndex.maxBy(_._1)
          if (max._1 > confThres) {
            println(max)
            println(labels(max._2))
            val (midX, midY) = ((data(0) * image.cols).toInt, (data(1) * image.rows).toInt)
            val (w, h) = ((data(2) * image.cols).toInt, (data(3) * image.rows).toInt)
            val (l, u) = (midX - w / 2, midY - h / 2)
            ids = max._2 :: ids
            confs = max._1 :: confs
            rects = new Rect(l, u, w, h) :: rects
          }
        }
      }
      val boxes = new MatOfRect(rects: _*)
      val confMat = new MatOfFloat(confs: _*)
      val idxMat = new MatOfInt()
      Dnn.NMSBoxes(boxes, confMat, confThres, nmsThres, idxMat)
      for (idx <- idxMat.toArray) {
        val box = rects(idx)
        Imgproc.rectangle(image, box.tl, box.br, new Scalar(0, 0, 255), 2)
        Imgproc.putText(image, s"${labels(ids(idx))}", box.tl, Imgproc.FONT_HERSHEY_SIMPLEX, 1, new Scalar(0, 0, 0))
      }
      Imgcodecs.imwrite("out.jpg", image)
      if (idxMat.total() == 0) {
        Ok(" \n \n")
      } else {
        val idxArr = idxMat.toArray
        val unionRect = rects.map { r =>
          new Rect(
            new Point(r.tl.x - r.width / 4.0, r.tl.y - r.height / 4.0),
            new Point(r.br.x + r.width / 4.0, r.br.y + r.height / 4.0),
          )
        }
        val dfu = new UnionFind(rects.size)
        for (i <- idxArr) {
          for (j <- idxArr) {
            val (x, y) = (unionRect(i), unionRect(j))
            val intersect = for {
              k1 <- Seq(0, 1)
              k2 <- Seq(0, 1)
            } yield {
              x.contains(new Point(y.tl.x + y.width * k1, y.tl.y + y.height * k2)) ||
              y.contains(new Point(x.tl.x + x.width * k1, x.tl.y + x.height * k2)) 
            }
            if (intersect.exists(b => b)) dfu.join(i, j)
          }
        }
        val topRightOrd = Ordering.by((x: Int) => rects(x).y - rects(x).x)
        val agariId = idxArr.reduceOption(topRightOrd.min)
        val agari = agariId.map(id => labels(ids(id))).getOrElse("")
        val answer = idxArr.groupBy(id => dfu.find(id)).flatMap { group =>
          val str = group._2.map(id => labels(ids(id))).mkString
          group._2 match {
            case seq if agariId.map(id => seq.contains(id)).getOrElse(false) =>
              str
            case seq if str.contains("bb") =>
              s"[${str}]"
            case seq if seq.size == 3 || seq.size == 4 =>
              s"(${str})"
            case _ =>
              str
          }
        }.mkString + s"\n${agari}\n"
        println(s"Time: ${net.getPerfProfile(new MatOfDouble()) / Core.getTickFrequency()}")
        Ok(answer)
      }
    }
}
