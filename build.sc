import mill._, scalalib._, scalajslib._, scalafmt._
import $file.riichala.build

def scalaVer = "2.12.8"
def scalaJSVer = "0.6.26"

object jsapp extends ScalaJSModule with ScalafmtModule {
	def scalaVersion = scalaVer
	def scalaJSVersion = scalaJSVer
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def moduleDeps = Seq(riichala.build.coreJs)
	def ivyDeps = Agg(
		ivy"com.thoughtworks.binding::dom_sjs0.6:11.6.0",
		ivy"com.thoughtworks.binding::binding_sjs0.6:11.6.0",
		ivy"org.scala-js::scalajs-dom_sjs0.6:0.9.6")
	def scalacPluginIvyDeps =
		super.scalacPluginIvyDeps() ++
		Agg(ivy"org.scalamacros:::paradise:2.1.0")
}

object server extends ScalaModule with ScalafmtModule {
	def scalaVersion = scalaVer
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def ivyDeps = Agg(ivy"com.github.finagle::finch-core:0.27.0")
	def unmanagedClasspath =  T {
		if (!ammonite.ops.exists(millSourcePath / "lib")) Agg()
		else Agg.from(PathRef(millSourcePath / "resources") +: ammonite.ops.ls(millSourcePath / "lib").map(PathRef(_)))
	}
	override def compile = T {
		jsapp.fullOpt()
		os.copy.over(os.pwd / "out" / "jsapp" / "fullOpt" / "dest" / "out.js", millSourcePath / "resources" / "app.js")
		os.copy.over(jsapp.millSourcePath / "resources" / "index.html", millSourcePath / "resources" / "index.html")
		super.compile()
	}
}
